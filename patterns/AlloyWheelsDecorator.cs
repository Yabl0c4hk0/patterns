﻿namespace patterns;

public class AlloyWheelsDecorator : CarDecorator
{
    public AlloyWheelsDecorator(Car car) : base(car)
    {
    }

    public override void DisplayInfo()
    {
        base.DisplayInfo();
        Console.WriteLine("Alloy Wheels: Yes");
    }
}