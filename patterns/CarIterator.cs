﻿namespace patterns;

public interface IAbstractIterator
{
    Car First();
    Car? Next();
    bool IsDone() => throw new NotImplementedException();
    Car CurrentItem() => throw new NotImplementedException();
}

public class CarIterator : IAbstractIterator
{
    CarCollection _collection;
    int _current = 0;
    int _step = 1;

    public CarIterator(CarCollection collection)
    {
        _collection = collection;
    }

    public Car First()
    {
        _current = 0;
        return _collection[_current];
    }

    public Car? Next()
    {
        _current += _step;
        return IsDone ? null : _collection[_current];
    }

    public bool IsDone => _current >= _collection.Count;

    public Car CurrentItem() => _collection[_current];
}