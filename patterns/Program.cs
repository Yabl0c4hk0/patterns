﻿namespace patterns;

// Builder Pattern

// Decorator Pattern

// Iterator Pattern

internal static class Program
{
    private static void Main()
    {
        // Builder example
        var carBuilder = new CarBuilder()
            .SetBrand("Toyota")
            .SetModel("Camry")
            .SetColor("Red")
            .SetSunroof(true)
            .SetGps(false);
        var sedan = carBuilder.GetCar();
        sedan.DisplayInfo();

        // Decorator example
        CarDecorator sedanWithLeatherSeats = new LeatherSeatsDecorator(sedan);
        CarDecorator sedanWithAlloyWheels = new AlloyWheelsDecorator(sedan);
        CarDecorator sedanWithLeatherSeatsAndAlloyWheels = new AlloyWheelsDecorator(new LeatherSeatsDecorator(sedan));

        Console.WriteLine("Sedan with Leather Seats:");
        sedanWithLeatherSeats.DisplayInfo();

        Console.WriteLine("Sedan with Alloy Wheels:");
        sedanWithAlloyWheels.DisplayInfo();

        Console.WriteLine("Sedan with Leather Seats and Alloy Wheels:");
        sedanWithLeatherSeatsAndAlloyWheels.DisplayInfo();

        // Iterator example
        var carCollection = new CarCollection();
        carCollection.AddCar(sedan);
        carCollection.AddCar(sedanWithLeatherSeats);
        carCollection.AddCar(sedanWithAlloyWheels);
        carCollection.AddCar(sedanWithLeatherSeatsAndAlloyWheels);

        Console.WriteLine("Car Collection:");
        var iterator = (CarIterator)carCollection.CreateIterator();
        
        for (var car = iterator.First();
            !iterator.IsDone; car = iterator.Next())
        {
            car.DisplayInfo();
        }
    }
}