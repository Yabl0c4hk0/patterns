﻿namespace patterns;

public interface  IAbstractCollection
{
    IAbstractIterator CreateIterator();
}

public class CarCollection: IAbstractCollection
{
    List<Car> _cars = new();
    
    public IAbstractIterator CreateIterator() => new CarIterator(this);
    
    public int Count => _cars.Count;
    
    public Car this[int index]
    {
        get => _cars[index];
        set => _cars.Add(value);
    }
    
    public void AddCar(Car car) => _cars.Add(car);
    
}