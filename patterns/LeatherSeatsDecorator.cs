﻿namespace patterns;

public class LeatherSeatsDecorator : CarDecorator
{
    public LeatherSeatsDecorator(Car car) : base(car)
    {
    }

    public override void DisplayInfo()
    {
        base.DisplayInfo();
        Console.WriteLine("Leather Seats: Yes");
    }
}