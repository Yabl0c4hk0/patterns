﻿namespace patterns;

public class Car
{
    public string Brand { get; set; }
    public string Model { get; set; }
    public string Color { get; set; }
    public bool HasSunroof { get; set; }
    public bool HasGps { get; set; }

    public virtual void DisplayInfo()
    {
        Console.WriteLine(
            $"Brand: {Brand}, Model: {Model}, Color: {Color}, Sunroof: {(HasSunroof ? "Yes" : "No")}, GPS: {(HasGps ? "Yes" : "No")}");
    }
}