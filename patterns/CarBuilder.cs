﻿namespace patterns;

public class CarBuilder : ICarBuilder
{
    private readonly Car _car;

    public CarBuilder() => _car = new Car();

    public ICarBuilder SetBrand(string brand)
    {
        _car.Brand = brand;
        return this;
    }

    public ICarBuilder SetModel(string model)
    {
        _car.Model = model;
        return this;
    }

    public ICarBuilder SetColor(string color)
    {
        _car.Color = color;
        return this;
    }

    public ICarBuilder SetSunroof(bool hasSunroof)
    {
        _car.HasSunroof = hasSunroof;
        return this;
    }

    public ICarBuilder SetGps(bool hasGps)
    {
        _car.HasGps = hasGps;
        return this;
    }

    public Car GetCar() => _car;
}