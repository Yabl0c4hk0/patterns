﻿namespace patterns;

public interface ICarBuilder
{
    ICarBuilder SetBrand(string brand);
    ICarBuilder SetModel(string model);
    ICarBuilder SetColor(string color);
    ICarBuilder SetSunroof(bool hasSunroof);
    ICarBuilder SetGps(bool hasGps);
    Car GetCar();
}