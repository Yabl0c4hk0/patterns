﻿namespace patterns;

public abstract class CarDecorator : Car
{
    protected Car Car;

    public CarDecorator(Car car)
    {
        Car = car;
    }

    public override void DisplayInfo()
    {
        Car.DisplayInfo();
    }
}